class Overlay {
  _overlay;

  constructor() {
    this._createOverlay();
    document.body.append(this._overlay);
  }
  _createOverlay() {
    this._overlay = document.createElement("div");
    this._overlay.setAttribute("id", "cfh-overlay");
    this._overlay.classList.add("hide");
  }

  open() {
    this._overlay.classList.remove("hide");
  }

  close() {
    this._overlay.classList.add("hide");
  }
}

export const overlay = new Overlay();

class Prompt extends Overlay {
  #okButton;
  #cancelButton;
  #closeButton;
  #message;

  #content;

  #okButtonCallback;
  #cancelButtonCallback;
  constructor() {
    super();
    this.#createContent();
    this.#createMessageBox();
  }

  open(
    message,
    okButtonCallback,
    cancelButtonCallback,
    okButtonText = "OK",
    cancelButtonText = "Cancel"
  ) {
    this.#message.innerText = message;
    this.#okButton.innerText = okButtonText;
    this.#cancelButton.innerText = cancelButtonText;
    this.#okButtonCallback = okButtonCallback;
    this.#cancelButtonCallback = cancelButtonCallback;

    this.#okButton.onclick = this.okButtonClicked.bind(this);

    this.#cancelButton.onclick = this.cancelButtonClicked.bind(this);
    this.#closeButton.onclick = this.cancelButtonClicked.bind(this);
    this._overlay.classList.remove("hide");
  }

  close() {
    this._overlay.classList.add("hide");
    this.#okButton.onclick = null;
    this.#cancelButton.onclick = null;
    this.#closeButton.onclick = null;
  }
  okButtonClicked() {
    if (!this.#okButtonCallback) return;
    this.#okButtonCallback();
  }
  cancelButtonClicked() {
    this.close();
    if (!this.#cancelButtonCallback) return;
    this.#cancelButtonCallback();
  }
  _createOverlay() {
    super._createOverlay();
  }

  #createContent() {
    this.#content = document.createElement("div");
    this.#content.classList.add("cfh-content");
    this._overlay.append(this.#content);
  }

  #createMessageBox() {
    const messageBox = document.createElement("div");
    const messageArea = document.createElement("div");
    const buttonArea = document.createElement("div");

    this.#message = document.createElement("p");
    this.#okButton = document.createElement("button");
    this.#cancelButton = document.createElement("button");
    this.#closeButton = document.createElement("button");

    this.#okButton.classList.add("cfh-ok");
    this.#cancelButton.classList.add("cfh-cancel");
    this.#closeButton.classList.add("cfh-close");

    messageBox.classList.add("cfh-message-box");
    messageArea.classList.add("cfh-message-area");
    buttonArea.classList.add("cfh-button-area");

    this.#okButton.classList.add("button", "aquamarine-background");
    this.#cancelButton.classList.add("button");

    messageArea.append(this.#message);
    buttonArea.append(this.#okButton);
    buttonArea.append(this.#cancelButton);

    messageBox.append(messageArea);
    messageBox.append(buttonArea);
    messageBox.append(this.#closeButton);

    this.#content.append(messageBox);
  }
}
export const prompt = new Prompt();
